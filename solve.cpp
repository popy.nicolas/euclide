#include<iostream>
#include<cstring>
#include<cmath>
#include<vector>
#include<tuple>
#include<utility>

/*
 * Décomposition en produit de facteur premier
 * Si on fait les divisions succéssives par l'ensemble des diviseurs 
 * que l'on rencontre en parcourant N dans l'ordre croissant 
 * alors la factorisation est forcement en nombre premier.
 * (si on divise par 3 jusqua que se ne soit plus possible, 
 * alors on ne peut diviser par aucun autre multiple de 3)
 *
 * TODO: Implementer : https://primes.utm.edu/notes/faq/six.html
 */
std::vector<std::pair<int,int>> decompositionFacteurPremier(unsigned long n){
	std::vector<std::pair<int,int>> result;
	int factor = 2, puissance = 0;
	if ( n % factor == 0){
		while(n % factor == 0) {
			n /= factor;
			puissance++;
		}
		result.push_back(std::make_pair(factor,puissance));
	}
	factor = 3;
	// le plus grand facteur est forcement, au plus, la racine carré du nombre
	int maxFactor = std::sqrt(n);
	while (n > 1 and factor <= maxFactor){
		if(n % factor == 0){
			puissance = 0;
			while(n % factor == 0){
				n /= factor;
				puissance++;
			}
			result.push_back(std::make_pair(factor,puissance));
			maxFactor = std::sqrt(n);
		}
		// ayant deja essayer 2 on sait que aucun autre nombre pair est premier inutile de tester.
		factor += 2;
	}
	// le reste est premier
	if(n != 1) result.push_back(std::make_pair(n,1));
	return result;
}

/*
 * calcul du pgcd par l'algorithme d'Euclide
 * Je ne sais pas comment on implement memoization en cpp
 */
int pgcd_euclide(int a,int b, std::vector<std::tuple<int, int, int, int>>* v){
	int q, r;
	q = a/b; r = a%b;
	v->push_back(std::make_tuple(a,b,q,r));
	return r>0 ? pgcd_euclide(b, r, v): b;
}

/*
 * calcul du ppcm par le resultat obtenue avec l'algorithme de euclide
 */
int ppcm_euclide(int a, int b, int pgcd){
	long long result = (long long) a*b/pgcd;
	return result;
}

/*
 * calcul du ppcm depuis la factorisation en nombre premier
 * A \lor B
 * TODO: renvoyer un vector de pair<int, int> pour sortir l'affichage de cette fonction
 */
std::vector<std::pair<int, int>> facteurs_premiers_ppcm(
		std::vector<std::pair<int, int>> a, 
		std::vector<std::pair<int, int>> b){
	std::vector<std::pair<int, int>> r;
	int j = 0;
	
	for(int i = 0; i < a.size(); i++){
		while(std::get<0>(a[i]) > std::get<0>(b[j]) && j<b.size()){
			r.push_back(b[j]);
			j++;
		}
		if(std::get<0>(a[i]) == std::get<0>(b[j])){
			if (std::get<1>(a[i]) >= std::get<1>(b[j])) r.push_back(a[i]);
			else r.push_back(b[j]);
			j++;
		} else {
			r.push_back(a[i]);
		}

	}
	if(j < b.size()) {
		for(;j < b.size();j++){
			r.push_back(b[j]);
		}
	}
	return r;
}

/*
 * liste des facteurs premier du pgcd de a et de b
 * Prend comme argument des vector<pair<int, int>>
 * et retourne un vector<pair<int, int>> 
 */
std::vector<std::pair<int, int>> facteursPremierPgcd(
		std::vector<std::pair<int, int>> a, 
		std::vector<std::pair<int, int>> b){
	std::vector<std::pair<int, int>> f;
	int j = 0, result = 1;
	for (int i = 0; i < a.size(); i++) {
		while(std::get<0>(a[i]) > std::get<0>(b[j]) && j<b.size()){
			j++;
		} if(std::get<0>(a[i]) == std::get<0>(b[j])){
			f.push_back(std::make_pair(
				std::get<0>(a[i]), 
				std::min(std::get<1>(a[i]),std::get<1>(b[j]))));
			j++;
		}
	} 
	return f;
}

/*
 * Calcul le produit de tout les facteurs.
 * Prend comme argument un vector<pair<int,int>>
 * retourne un long.
 */
long produit_facteurs(std::vector<std::pair<int, int>> f){
	long result = 1;
	for(std::pair<int, int> p : f){
		result *= std::pow(std::get<0>(p), std::get<1>(p));
	}
	return result;
}


/*
 * Une chaine de caractere est elle la représentation d'un nombre ?
 * https://stackoverflow.com/questions/29248585/c-checking-command-line-argument-is-integer-or-not
 */
bool estUnNombre(char* c){
	int i = 0;
	bool result = true;
	if (c[0] == '-') i = 1;
	for (i;i<strlen(c);i++){
		if (!isdigit(c[i])){
			result = false;
			break;
		}
	}
	return result;
}


/*
 * Verifie si les paramêtre passé sont de forme valide
 */
bool argvEstExploitable(int argc, char** argv){
	if(argc > 2){
		if(!estUnNombre(argv[1])){
			std::cout<<argv[1]<<std::endl;
			if(argc == 3){
				return false;
			}
			if(strcmp(argv[1],"-l")!=0){
				return false;
			}
		} else {
			if(!estUnNombre(argv[2])){
				return false;
			}
		} 
	}
	return true;
}

/*
 * Fonction d'affichage du vector contenant la décomposition en facteur premier
 */
void afficherDecomposition(std::vector<std::pair<int,int>> v, int l=0){
	if(v.size() == 0){
		std::cout << "1";
	}
	for(int i = 0; i<v.size(); i++){
		std::cout << std::get<0>(v[i]);
			if(std::get<1>(v[i]) > 1 ) std::cout << "^" << std::get<1>(v[i]);
		if(i < v.size()-1){
			if(l) std::cout<<"\\cdot ";
			else std::cout<<" * ";
		}
	}
}

/*
 * Affiche les étapes de l'alogorithme d´Euclide.
 */
void afficherEtapesEuclide(std::vector<std::tuple<int,int,int,int>> v, int l=0){
	const char *multiplie, *alignement, *endline;
	if(l) {
		multiplie = "\\times ";
		alignement = "&";
		endline = "\\\\ ";
		std::cout<<"\\begin{aligned}"<<std::endl;
	} else {
		multiplie = "*";
		alignement = "";
		endline = "";
	}
	for(int i = 0; i< v.size(); i++){
		std::cout << std::get<0>(v[i]) << alignement << "="
			<< std::get<1>(v[i]) << multiplie << std::get<2>(v[i]) <<
			"+" << std::get<3>(v[i]);
		if(i != v.size()-1){
			std::cout<<endline;
		}
		std::cout << std::endl;
	}
	if(l){
		std::cout<<"\\end{aligned}"<<std::endl;
	}
}
/*
 * Afficher la sortie pour une paire a,b
 */
void generer_output(int a, int b, int l){
	int pgcd_e, max_op;
	std::vector<std::pair<int,int>> va, vb, pgcd_fp, ppcm_fp;
	std::vector<std::tuple<int,int,int,int>> etape_pgcd_e;
	if(l) std::cout<< "\\item " << a << ", " << b << ":" <<std::endl
		<< "\\begin{enumerate}" << std::endl << "\\item " 
		<< "Décomposition en facteurs premiers." << std::endl;

	va = decompositionFacteurPremier(a);
	vb = decompositionFacteurPremier(b);


	// 1 - Décomposition en facteurs premiers
	if(l) std::cout<< "\\begin{align*}" << std::endl << a << "&=";
	afficherDecomposition(va, l);
	if(l) std::cout << " \\\\" << std::endl << b << "&=";
	afficherDecomposition(vb, l);
	if(l) std::cout<< std::endl << "\\end{align*}" << std::endl;

	// 2 - pgcd ppcm produit de facteurs premiers
	pgcd_fp = facteursPremierPgcd(va,vb);
	if(l) std::cout<<"\\item PGCD et PPCM depuis la décomposition en facteurs premiers." 
		<< std::endl << "\\begin{align*}" << std::endl;
	std::cout<<"pgcd("<<a<<","<<b<<") &= " ;
	afficherDecomposition(pgcd_fp, l) ;
	if(pgcd_fp.size() > 1 || (pgcd_fp.size() == 1 && std::get<1>(pgcd_fp[0]) != 1)) {
		std::cout<< "=" << produit_facteurs(pgcd_fp);
	}
	if(l) std::cout<<" \\\\";
	std::cout<<std::endl;

	ppcm_fp = facteurs_premiers_ppcm(va,vb);
	std::cout << "ppcm(" << a << "," << b << ") &= ";  
	afficherDecomposition(ppcm_fp,l);
	if(ppcm_fp.size() > 1) {
		std::cout<< "=" << produit_facteurs(ppcm_fp);
	}
	if(l) std::cout<< std::endl << "\\end{align*}" << std::endl;
	std::cout<<std::endl;

	// 3 - Euclide
	pgcd_e = pgcd_euclide(a,b, &etape_pgcd_e);
	if(l) std::cout<<"\\item PGCD et PPCM avec l'algorithme d'EUCLIDE." 
		<< std::endl << "\\begin{gather*}";
	afficherEtapesEuclide(etape_pgcd_e,l);
	if(l) std::cout<<"\\\\[6pt]"<<std::endl;

	if(l) std::cout << "\\begin{aligned}" << std::endl;
	std::cout<<"pgcd("<<a<<","<<b<<") &= "<< pgcd_e;
	std::cout<< " \\\\" << std::endl;

	std::cout<<"ppcm("<<a<<","<<b<<") &= "<< ppcm_euclide(a,b,pgcd_e);
	if(l) std::cout<< std::endl << "\\end{aligned}\\end{gather*}" << std::endl;
	std::cout<<std::endl;

	//4 - Lamé
	max_op =  ((int)std::log10(std::max(a,b)) + 1) * 5;
	if(l) std::cout<<"\\item Nombre d'étape nécéssaire "
		<< "pour obtenir le pgcd par l'algorithme d'EUCLIDE." << std::endl << std::endl
			<< "$" << etape_pgcd_e.size() << "$ étapes ont été nécéssaires pour obtenir" 
			<< " le PGCD par l'algorithme d'EUCLIDE." << std::endl <<std::endl
			<< " Le théorème de LAMÉ nous permet d'affirmer qu'il faut au maximum : " 
			<< "$5 \\cdot " <<((int)std::log10(std::max(a,b)) + 1)<< "=";
	// attention a 0 le cas n'est pas géré ?
	std::cout<<max_op<< "$" << std::endl
		<<  " divisions pour trouver le pgcd à l'aide de l'algorithme d'EUCLIDE."
		<<std::endl;
	if(l) std::cout<< "\\end{enumerate}" << std::endl ;
}

/*
 * Fonction principale appelant les fonction nécessaires.
 */
int main(int argc, char** argv){
	int a, b, l = 0, i, j, tc = 1, f;
	int ab[2];
	size_t p_end;
	if(argvEstExploitable(argc, argv)){
		if(!estUnNombre(argv[1])){
			l = 1;
		} 
		if(argc > 2){
			if(l == 0){
				j = 1;
			} else{
				j = 2;
			}
			a = strtol(argv[j], NULL, 10);
			b = strtol(argv[j+1], NULL, 10);
		} else {
			std::cin >> tc;
		}	
		if(l) std::cout << "\\begin{itemize}" << std::endl;
		for(int i = 0; i<tc; i++){
			if(argc <= 2) {
				scanf("%d%d", &a, &b);
			}
			generer_output(a, b, l);
		}
		if(l) std::cout << "\\end{itemize}" << std::endl;
	} else {
		std::cout<< "Usage: nom_du_programme [OPTION] a b" << std::endl 
			<< "Options:" << std::endl <<
			"\t-l\tformat la sortie en latex." << std::endl;
	}
	return 0;
}
